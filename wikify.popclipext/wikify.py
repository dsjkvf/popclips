#!/usr/bin/env python3


# LEGAL

__author__ = "dsjkvf"
__credits__ = []
__license__ = "Revised BSD"
__maintainer__ = "dsjkvf"
__email__ = "dsjkvf@gmail.com"
__status__ = "Production"
__version__ = "1.0.0"
__url__ = "https://bitbucket.org/dsjkvf/popclips/"

# IMPORT MODULES

import os

# DEFINE EVENT HANDLER FUNCTIONS

def wiki():
    url = 'https://' + lang_code + '.wikipedia.org/w/index.php?title=Special:Search&search=' + selected_text
    # add quotes to the final link because of the `&` character
    os.system("open " + "'%s'" % url)

# START EVERYTHING

# Get the text selected by PopClip
selected_text = os.environ['POPCLIP_TEXT']
# Get the key pressed
key = os.environ['POPCLIP_MODIFIER_FLAGS']
# Call a coresponding function according to the key pressed
if key == '1048576':
    lang_code = os.environ['POPCLIP_OPTION_WICMD']
    wiki()
elif key == '262144':
    lang_code = os.environ['POPCLIP_OPTION_WICTR']
    wiki()
elif key == '524288':
    lang_code = os.environ['POPCLIP_OPTION_WIOPT']
    wiki()
else:
    lang_code = os.environ['POPCLIP_OPTION_WINON']
    wiki()
