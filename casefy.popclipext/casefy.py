#!/usr/bin/env python3


# LEGAL

__author__ = "dsjkvf"
__credits__ = []
__license__ = "Revised BSD"
__maintainer__ = "dsjkvf"
__email__ = "dsjkvf@gmail.com"
__status__ = "Production"
__version__ = "1.0.0"
__url__ = "https://bitbucket.org/dsjkvf/popclips/"


# IMPORT MODULES

import sys
import os


# DEFINE EVENT HANDLER FUNCTIONS

def low():
    sys.stdout.write(selected_text.decode('utf-8').lower().encode('utf-8'))

def upp():
    sys.stdout.write(selected_text.decode('utf-8').upper().encode('utf-8'))

def tit():
    sys.stdout.write(selected_text.decode('utf-8').title().encode('utf-8'))


# REGISTER EVENT HANDLERS (CODE THAT CALLS UP EVENT HANDLER FUNCTIONS)

# Get the user set values from the extension's settings
non = os.environ['POPCLIP_OPTION_CSNON']
cmd = os.environ['POPCLIP_OPTION_CSCMD']
ctr = os.environ['POPCLIP_OPTION_CSCTR']
# Transform the settings into the functions name
func_cmd = globals()[cmd]
func_ctr = globals()[ctr]
func_non = globals()[non]


# START EVERYTHING

# Get the text selected by PopClip
selected_text = os.environ['POPCLIP_TEXT']
# Get the key pressed
key = os.environ['POPCLIP_MODIFIER_FLAGS']
# Call a coresponding function according to the key pressed
if key == '1048576':
    func_cmd()
elif key == '262144':
    func_ctr()
else:
    func_non()
