#!/usr/bin/env python3

# Markdown tags for PopClip
# By dsjkvf (2013) dsjkvf@gmail.com

import sys
import os

def em():
	sys.stdout.write ("*" + selected_text + "*")

def strong():
	sys.stdout.write ("**" + selected_text + "**")

def quote():
	sys.stdout.write ("> " + selected_text)

def code():
	sys.stdout.write ("`" + selected_text + "`")

def tab():
	sys.stdout.write ("    " + selected_text)
	
def comment():
	sys.stdout.write ("<!-- " + selected_text + " -->")

# get the text selected by PopClip
selected_text = os.environ['POPCLIP_TEXT']
# get the tags set for the corresponding keys (from the extension's settings)
cmd = os.environ['POPCLIP_OPTION_CMD']
opt = os.environ['POPCLIP_OPTION_OPT']
ctr = os.environ['POPCLIP_OPTION_CTR']
non = os.environ['POPCLIP_OPTION_NON']
# special keys: Command + Option, Command + Control
cmo = os.environ['POPCLIP_OPTION_CMO']
cmr = os.environ['POPCLIP_OPTION_CMR']
# transform the settings into the functions name
func_cmd = globals()[cmd]
func_opt = globals()[opt]
func_ctr = globals()[ctr]
func_non = globals()[non]
# special keys: Command + Option, Command + Control
func_cmo = globals()[cmo]
func_cmr = globals()[cmr]
# get the key pressed
key = os.environ['POPCLIP_MODIFIER_FLAGS']
if key == '1572864':
	func_cmo()
elif key == '1310720':
	func_cmr()
elif key == '1048576':
	func_cmd()
elif key == '524288':
	func_opt()
elif key == '262144':
	func_ctr()
else:
	func_non()
