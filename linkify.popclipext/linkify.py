#!/usr/bin/env python3


# LEGAL

__author__ = "dsjkvf"
__credits__ = []
__license__ = "Revised BSD"
__maintainer__ = "dsjkvf"
__email__ = "dsjkvf@gmail.com"
__status__ = "Production"
__version__ = "1.0.2"
__url__ = "https://bitbucket.org/dsjkvf/popclips/"


# IMPORT MODULES

import sys
import os
import re
import subprocess
import requests
from bs4 import BeautifulSoup

# DEFINE HELPERS

def lucky(search_string, search_site):
     search_query = "https://www.google.com/search?q=" + search_string + "%20" + search_site
     res = requests.get(search_query)
     res = res.content
     res = str(BeautifulSoup(res, "html.parser").findAll('a', attrs={'href': re.compile("^\/url\?q\=http")})[0].get('href'))
     return re.sub('^.*(http.*?)\&.*', '\\1', res)

# DEFINE EVENT HANDLER FUNCTIONS

def empty():
    sys.stdout.write ("[" + selected_text + "]()")

def clippy():
    clip = subprocess.Popen(['pbpaste'], stdout=subprocess.PIPE).stdout.read()
    sys.stdout.write ("[" + selected_text + "](" + str(clip.decode('utf-8')) + ")")

def lucky_wiki():
    sys.stdout.write ("[" + selected_text + "](" + \
            str(lucky(selected_text, "site:wikipedia.org")) + ")")

def lucky_goog():
    sys.stdout.write ("[" + selected_text + "](" + \
            str(lucky(selected_text, "")) + ")")


# REGISTER EVENT HANDLERS (CODE THAT CALLS UP EVENT HANDLER FUNCTIONS)

# Get the user set values from the extension's settings
non = os.environ['POPCLIP_OPTION_LINON']
cmd = os.environ['POPCLIP_OPTION_LICMD']
ctr = os.environ['POPCLIP_OPTION_LICTR']
opt = os.environ['POPCLIP_OPTION_LIOPT']
# Transform the settings into the functions name
func_cmd = globals()[cmd]
func_ctr = globals()[ctr]
func_opt = globals()[opt]
func_non = globals()[non]


# START EVERYTHING

# Get the text selected by PopClip
selected_text = os.environ['POPCLIP_TEXT']
# Get the key pressed
key = os.environ['POPCLIP_MODIFIER_FLAGS']
# Call a coresponding function according to the key pressed
if key == '1048576':
    func_cmd()
elif key == '262144':
    func_ctr()
elif key == '524288':
    func_opt()
else:
    func_non()
