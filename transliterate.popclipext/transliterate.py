#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# LEGAL

# Header
__author__ = "dsjkvf"
__credits__ = []
__license__ = "Revised BSD"
__maintainer__ = "dsjkvf"
__email__ = "dsjkvf@gmail.com"
__status__ = "Production"
__version__ = "1.0.0"
__url__ = "https://bitbucket.org/dsjkvf/popclips/"

# MODULES

# Import
import os
import codecs
import sys

# HELPERS

# Build the transliteration dictionaries
def read_keyb(option):
    d1 = {}
    d2 = {}
    with codecs.open("keyb-" + str(option) + ".txt", encoding = 'UTF8') as f:
        for line in f:
                k1, v1, k2, v2 = line.split()
                d1[k1] = v1
                d2[k2] = v2
    return (d1, d2)

# MAIN

# Absorb inputs
# get the text selected by PopClip
selected_text = os.environ['POPCLIP_TEXT']
# get the keyboard option
keyboard = os.environ['POPCLIP_OPTION_KEYBRD']
# read keyboards
engrus, ruseng = read_keyb(keyboard)
allowed_eng = engrus.keys()
allowed_rus = ruseng.keys()
allowed = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', \
           ',', '.', '/', '|', ' ', "'", '"', ';', ':', '!', \
           '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', \
           '_', '=', '+', '[', '{', ']', '}', '№', '`', '~', \
           '\\', '<', '>']
# Proceed with the transformation
st = selected_text.decode('UTF8')
rz = ''
for i in range(len(st)):
    # if from English to Russian
    if st[i] in allowed_eng:
        rz += engrus[st[i]]
    # if from Russian to English
    elif st[i] in allowed_rus:
        # mysterious "Й -> Ĭ; и -> ĭ" bug
        try:
            if st[i] =="и".decode('UTF8') and st[i+1] not in allowed_rus and st[i+1] not in allowed:
                rz += "j"
            elif st[i] =="И".decode('UTF8') and st[i+1] not in allowed_rus and st[i+1] not in allowed:
                rz += "J"
            else:
                rz += ruseng[st[i]]
        except IndexError:
            rz += "i"
    # if a number or a symbol
    elif st[i] in allowed:
        rz += st[i]
    # again that mysterious "Й -> Ĭ; и -> ĭ" bug
    else:
        continue
# Encode
rz = rz.encode('UTF8')
# Return the result
sys.stdout.write(rz)
