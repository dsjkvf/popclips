#!/usr/bin/env python3


# LEGAL

__author__ = "dsjkvf"
__credits__ = []
__license__ = "Revised BSD"
__maintainer__ = "dsjkvf"
__email__ = "dsjkvf@gmail.com"
__status__ = "Production"
__version__ = "1.0.0"
__url__ = "https://bitbucket.org/dsjkvf/popclips/"


# IMPORT MODULES

import os
import webbrowser


# DEFINE EVENT HANDLER FUNCTIONS

def yahoo():
    url = 'http://finance.yahoo.com/q?s=' + selected_text
    webbrowser.open(str(url))

def finviz():
    url = 'http://finviz.com/quote.ashx?t=' + selected_text
    webbrowser.open(str(url))

def seekal():
    url = 'http://seekingalpha.com/symbol/' + selected_text
    webbrowser.open(str(url))


# REGISTER EVENT HANDLERS (CODE THAT CALLS UP EVENT HANDLER FUNCTIONS)

# Get the user set values from the extension's settings
non = os.environ['POPCLIP_OPTION_MONNON']
cmd = os.environ['POPCLIP_OPTION_MONCMD']
ctr = os.environ['POPCLIP_OPTION_MONCTR']
# Transform the settings into the functions name
func_cmd = globals()[cmd]
func_ctr = globals()[ctr]
func_non = globals()[non]


# START EVERYTHING

# Get the text selected by PopClip
selected_text = os.environ['POPCLIP_TEXT']
# Get the key pressed
key = os.environ['POPCLIP_MODIFIER_FLAGS']
# Call a coresponding function according to the key pressed
if key == '1048576':
    func_cmd()
elif key == '262144':
    func_ctr()
else:
    func_non()
