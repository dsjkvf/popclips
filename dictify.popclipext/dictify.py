#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# LEGAL

__author__ = "dsjkvf"
__credits__ = []
__license__ = "Revised BSD"
__maintainer__ = "dsjkvf"
__email__ = "dsjkvf@gmail.com"
__status__ = "Production"
__version__ = "1.1.0"
__url__ = "https://bitbucket.org/dsjkvf/popclips/"


# IMPORT MODULES

import os
# import webbrowser


# DEFINE EVENT HANDLER FUNCTIONS

def mtran():
    url = 'http://www.multitran.ru/c/m.exe?s=' + selected_text
#     webbrowser.open(str(url))
    os.system("open " + "'%s'" % url)

def urban():
    url = 'http://www.urbandictionary.com/define.php?term=' + selected_text
#     webbrowser.open(str(url))
    os.system("open " + "'%s'" % url)

def synonym():
    url = 'http://thesaurus.com/browse/' + selected_text
#     webbrowser.open(str(url))
    os.system("open " + "'%s'" % url)


# REGISTER EVENT HANDLERS (CODE THAT CALLS UP EVENT HANDLER FUNCTIONS)

# Get the user set values from the extension's settings
non = os.environ['POPCLIP_OPTION_DICNON']
cmd = os.environ['POPCLIP_OPTION_DICCMD']
ctr = os.environ['POPCLIP_OPTION_DICCTR']
# Transform the settings into the functions name
func_cmd = globals()[cmd]
func_ctr = globals()[ctr]
func_non = globals()[non]


# START EVERYTHING

# Get the text selected by PopClip
selected_text = os.environ['POPCLIP_TEXT']
# Get the key pressed
key = os.environ['POPCLIP_MODIFIER_FLAGS']
# Call a coresponding function according to the key pressed
if key == '1048576':
    func_cmd()
elif key == '262144':
    func_ctr()
else:
    func_non()
