## PopClip.app plugins

Collected here are some plugins for the OS X application [PopClip](http://pilotmoon.com/popclip/). The first five below are Markdown helpers:

| Name           | Action with no key pressed | With CMD pressed                    | With CTRL pressed               | With OPT pressed                     |
|:--------------:|:--------------------------:|:-----------------------------------:|:-------------------------------:|:------------------------------------:|
| **emphify**    | emphasize text             | make text strong                    | quote text                      |                                      |
| **codify**     | add backticks for code     | add tab indent for code             | add HTML comment                |                                      |
| **casefy**     | change to upper case       | change to lower case                | capitalize                      |                                      |
| **headify**    | add header                 | add sub-header                      | add unordered list item         |                                      |
| **linkify**    | make an empty link         | make a link with clipboard contents | link to a Wikipedia entry       | link to Google's "I'm feeling lucky" |

Besides, **emphify** and **codify** extensions may be replaced by additionally provided **tagify** plugin, which accomplishes the same 6 transformations and therefore uses not 2 modificators (CMD and CTRL) but 5 (CMD, CTRl, OPT, CMD+CTRL, CMD + OPT).

The next few are different search engines:

| Name           | Action with no key pressed | With CMD pressed         | With OPT pressed      | With CTRL pressed     |
|:--------------:|:--------------------------:|:------------------------:|:---------------------:|:---------------------:|
| **wikify**     | search Wikipedia/EN        | adjustable parameter     | adjustable parameter  | adjustable parameter  |
| **dictify**    | search Multitran           | search UrbanDictionary   |                       | search Thesaurus      |
| **piratify**   | search RuTor               | search RuTracker         | search RuTracker/TV   | search TBP            |
| **stockify**   | search FinViz              | search Yahoo Finance     |                       | search SeekingAlpha   |
| **bookify**    | search Flibusta            | search LibGen            |                       |                       |

Also included the **googlify** extension, which -- no surprises -- performs a Google search but uses a SSL version and also includes the `pws=0` parameter, which removes history-based personalization, the `gl=us` parameter, which sets the country of origin to the United States, and the `gws_rd=cr` parameter, which redirects a user to a country specific Google domain -- the original `.com` in this case.

And as an alternative, also included the **startify** extension, which performs an internet search via privacy oriented StartPage search engine.

Besides, there's also the **transliterate** extension, which converts text entered in a wrong keyboard layout into a right one. For now, it only supports bidirectional conversion (i.e., no more than two layouts are considered) between English and Russian. Layouts are described in files named `keyb-<layout_name>.txt` inside the extension and, of course, may be freely adapted to one's needs.

To install these plugins simply [download](https://bitbucket.org/dsjkvf/popclips/downloads) the whole package, extract it, make sure the extension of every folder is `.popclipext` -- and then doubleclick it (or, actually, a plugin). And that's it.
