#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# LEGAL

__author__ = "dsjkvf"
__credits__ = []
__license__ = "Revised BSD"
__maintainer__ = "dsjkvf"
__email__ = "dsjkvf@gmail.com"
__status__ = "Production"
__version__ = "1.0.0"
__url__ = "https://bitbucket.org/dsjkvf/popclips/"

# IMPORT MODULES

import os

# DEFINE EVENT HANDLER FUNCTIONS

def flibst():
#     if 'й' in selected_text:
#         selected_text.replace('й', 'й')
    url = 'http://flibusta.net/booksearch?ask=' + selected_text
    os.system("open " + "'%s'" % url)

def libgen():
    url = 'http://libgen.io/foreignfiction/?s=' + selected_text
    os.system("open " + "'%s'" % url)

# def libtmp1():
#     pass
#
# def libtmp2():
#     pass

# REGISTER EVENT HANDLERS (CODE THAT CALLS UP EVENT HANDLER FUNCTIONS)

# Get the user set values from the extension's settings
non = os.environ['POPCLIP_OPTION_BOONON']
cmd = os.environ['POPCLIP_OPTION_BOOCMD']
# ctr = os.environ['POPCLIP_OPTION_BOOCTR']
# opt = os.environ['POPCLIP_OPTION_BOOOPT']
# Transform the settings into the functions name
func_cmd = globals()[cmd]
# func_ctr = globals()[ctr]
# func_opt = globals()[opt]
func_non = globals()[non]

# START EVERYTHING

# Get the text selected by PopClip
selected_text = os.environ['POPCLIP_TEXT']
# Get the key pressed
key = os.environ['POPCLIP_MODIFIER_FLAGS']
# Call a coresponding function according to the key pressed
if key == '1048576':
    func_cmd()
# elif key == '262144':
#     func_ctr()
# elif key == '524288':
#     func_opt()
else:
    func_non()
